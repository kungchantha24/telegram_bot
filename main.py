import requests;
from datetime import date

def telegram_bot_sent(mesage):
    bot_token = "6159170644:AAHVEkQgoO51XL0EcLooYnog4QOs_FP2K1o"
    # group_id = "-825405840" # Test
    group_id = "-826835557" # DL-Mobile
    try:
        url_sent = "https://api.telegram.org/bot" + str(bot_token) + "/sendMessage?chat_id=" + str(group_id)
        
        textdata = { "text" : mesage }
        response = requests.request("post",url=url_sent,params=textdata)
    except Exception as e:
        print (e)
        
def toKhmerNumber(num):
    numbersKhmer = ['០', '១', '២', '៣', '៤', '៥', '៦', '៧', '៨', '៩']
    khmerDate = ""
    for x in num: 
        khmerDate = khmerDate + numbersKhmer[int(x)]
    return khmerDate

dateNow = str(date.today()).split("-")
day =  str(int(dateNow[2])+int('1'))
month = dateNow[1]
year = dateNow[0]

monthDic = {
    "01" : "មករា",
    "02" : "កុម្ភៈ",
    "03" : "មីនា",
    "04" : "មេសា",
    "05" : "ឧសភា",
    "06" : "មិថុនា",
    "07" : "កក្កដា",
    "08" : "សីហា",
    "09" : "កញ្ញា",
    "10" : "តុលា",
    "11" : "វិច្ឆិកា",
    "12" : "ធ្នូ",
}

day = toKhmerNumber(str(day))
year = toKhmerNumber(str(year))
month = monthDic[month]

message = '''
យោងតាមការណែនាំរបស់ លោកប្រធាននាយកដ្ឋាន 

សូមគោរពអញ្ជើញ  
    - លោក បូរ៉ាត មុនីរត័ន៍ @Muniroath_Borath 
    - លោក គង់ ចន្ថា @Kung_Chantha 
    - លោក លី  ឡាយ @lay_ly 
    - កញ្ញា សូ ធានី @theany_so 
    - លោក អាង  ម៉េងអ័ង @AngMengOng 
    - កញ្ញា បុណ្យ ដានិច @Danich_B 
    - លោក វុទ្ធី គីមហុង @kimhongvuthy 
    - លោក បូរ ពិសី @bopisey 
    - លោក យិត បុណ្ណាស័ក្តិ @Yitsak 
    - លោក ទូច ហៀងងួន @Heangngoun_Touch 
    - លោក ឈឿន វឌ្ឍនៈ @Chh_Vathanak 
    - កញ្ញា លី ស៊ីវហ៊ួរ @ly_sivhour 

ដើម្បីចូលរួម៖ កិច្ចប្រជុំពិភាក្សា ដើម្បីពិនិត្យវឌ្ឍនភាពការងារ និងរបាយការណ៍ប្រចាំសប្តាហ៍ នៃការិយាល័យកម្មវិធីទូរស័ព្ទដៃ និងគេហទំព័រ។

ដឹកនាំដោយ៖ លោក ហ៊ន គឹមហាន 

ដែលនឹងប្រព្រឹត្តទៅនា៖ ថ្ងៃទីday ខែmonth ឆ្នាំyear វេលាម៉ោង ៨:០០ នាទីព្រឹក
ទីកន្លែង៖ នៅបន្ទប់ប្រជុំអគ្គនាយកដ្ឋាន ជាន់ផ្ទាល់ដី អគារ ក ៕

សូមអរគុណ!
'''

message = message.replace("day", day)
message = message.replace("month", month)
message = message.replace("year", year)
        
telegram_bot_sent(message)
